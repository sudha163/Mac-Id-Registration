import ConfigParser
import os
import sys
import requests
import re
from PyQt4 import QtGui, QtCore

macid = ''
IPAdd = ''
loginID = ''
pwd = ''

def findAddress():
    # Fethcing IP address and MAC ID from system
    '''Making call to ifconfig / ipconfig ssytem call and storing result in an list line wise. Serached patterns
    which are common in the blocks having legitimate data and fetched those uysing regex matching'''
    global macid
    global IPAdd
    pf = str(sys.platform)
    if pf[0] == 'w' or pf[0] == 'W':
        p = os.popen("ipconfig /all").readlines()
        le = len(p)
        block = []
        for i in range(0, le - 2, 1):
            if p[i] == '\n' and p[i + 2] == '\n':
                block.append(i + 1)
        block.append(le)
        getB = 0
        getE = 0
        chk = 0
        for j in range(0, len(block) - 1, 1):
            for k in range(block[j], block[j + 1], 1):
                if 'DNS Servers' in p[k]:
                    n = re.search(
                        r'(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)',
                        p[k])
                    if n is None:
                        pass
                    else:
                        y = str(n.group(0))
                        if y == '192.168.1.250':
                            getB = block[j]
                            getE = block[j + 1]
                            chk = 1
                            break
            if chk == 1:
                break
        if getB != 0 and getE != 0:
            for f in range(getB, getE, 1):
                if 'Physical Address' in p[f]:
                    g = re.search(r'(([0-9A-Fa-f]{2}[-:]){5}[0-9A-Fa-f]{2})|(([0-9A-Fa-f]{4}\.){2}[0-9A-Fa-f]{4})',
                                  p[f])
                    macid = g.group(0)
                if 'IPv4 Address' in p[f]:
                    h = re.search(
                        r'(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)',
                        p[f])
                    IPAdd = h.group(0)

                    # Algorithm to fetch only the IP address and MAC address

    if pf[0] == 'l' or pf[0] == 'L':
        p = os.popen("ifconfig").readlines()
        le = len(p)
        block = []
        for i in range(0, le - 1, 1):
            # print p[i]
            if 'Link encap' in p[i]:
                block.append(i)
        getB = -1
        getE = -1
        chk = 0
        for j in range(0, len(block) - 1, 1):
            for k in range(block[j], block[j + 1], 1):
                if 'inet addr' in p[k]:
                    t = re.search(
                        r'(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)',
                        p[k])
                    if '192.168' in str(t.group(0)):
                        getB = block[j]
                        getE = block[j + 1]
                        chk = 1
                        break
            if chk == 1:
                break
        if getB != -1 and getE != -1:
            for f in range(getB, getE, 1):
                if 'HWaddr' in p[f]:
                    g = re.search(r'(([0-9A-Fa-f]{2}[-:]){5}[0-9A-Fa-f]{2})|(([0-9A-Fa-f]{4}\.){2}[0-9A-Fa-f]{4})',
                                  p[f])
                    macid = g.group(0)
                if 'inet addr' in p[f]:
                    h = re.search(
                        r'(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)',
                        p[f])
                    IPAdd = h.group(0)
                    # Algorithm to fetch only the IP address and MAC address


def callService(logID,PWD,status):
    #Function to call the service for handling the client data
    global macid
    global IPAdd
    # Base URL being accessed
    url = 'http://localhost/macIDRKL/macip.php'
    # Dictionary of query parameters (if any)
    parms = {
        'loginID': logID,
        'pwd': PWD,
        'macid': macid,
        'ipaddr': IPAdd
    }
    status.setText("Working...")
    try:
        status.setText("Sending Data For Registration")
        resp = requests.get(url, params=parms)#Making request to service
        print resp.text
        if resp.text == 'OKAY':#If everything is successful
            status.setText("Registration Completed Successfully")
        else:
            if resp.text == 'WC':#If credentiasl are wrong
                status.setText("Wrong User Credentials. Check Again")
            else:#If error occurs in service
                status.setText("Registration Failed. If you have already registred\n You can't register again\n If not, Contact Network Administrator")
    except Exception as e:
        print e
        status.setText("Connection To Service Failed. Contact Network Administrator")


def exitFunc():
    sys.exit(0)

def mainWin():
    global macid
    global IPAdd
    app = QtGui.QApplication(sys.argv)

    mainWindow = QtGui.QMainWindow()
    mainWindow.setWindowTitle("NETWORK REGISTARTION - NIT ROURKELA")
    backgorundPallet = QtGui.QPalette()
    backgorundGradient = QtGui.QLinearGradient(QtCore.QRectF(mainWindow.rect()).topLeft(), QtCore.QRectF(mainWindow.rect()).bottomRight())
    backgorundGradient.setColorAt(0.0,QtCore.Qt.cyan)
    backgorundGradient.setColorAt(0.7, QtCore.Qt.darkCyan)
    backgorundPallet.setBrush(QtGui.QPalette.Background,QtGui.QBrush(backgorundGradient))
    mainWindow.setPalette(backgorundPallet)
    mainWindow.setFixedSize(600,350)
    mainWindow.setWindowIcon(QtGui.QIcon('NIT-Rourkela.jpg'))

    titleWin = QtGui.QLabel()
    titleWin.setText("NETWORK ACCESS REGISTARTION\nNIT ROURKELA")
    titleWin.setAlignment(QtCore.Qt.AlignCenter)
    titleWinFont = QtGui.QFont()
    titleWinFont.setBold(True)
    titleWinFont.setFamily("Algerian")
    titleWinFont.setPointSize(25)
    titleWin.setFont(titleWinFont)
    titleLayout = QtGui.QHBoxLayout()
    titleLayout.addWidget(titleWin)

    macIDLable = QtGui.QLabel()
    macIDLable.setText("MAC ID of your system: "+str(macid))
    IPAddLable = QtGui.QLabel()
    IPAddLable.setText("IP Address of your system on this network: "+str(IPAdd))
    addrLayout = QtGui.QVBoxLayout()
    addrLayout.addWidget(macIDLable)
    addrLayout.addWidget(IPAddLable)

    loginLabel = QtGui.QLabel()
    loginLabel.setText("Login ID   ")
    loginInput = QtGui.QLineEdit()
    loginLayout = QtGui.QHBoxLayout()
    loginLayout.addWidget(loginLabel)
    loginLayout.addWidget(loginInput)
    loginLayout.addStretch()
    pwdLabel = QtGui.QLabel()
    pwdLabel.setText("Password ")
    pwdInput = QtGui.QLineEdit()
    pwdInput.setEchoMode(QtGui.QLineEdit.Password)
    pwdLayout = QtGui.QHBoxLayout()
    pwdLayout.addWidget(pwdLabel)
    pwdLayout.addWidget(pwdInput)
    pwdLayout.addStretch()
    logLayoutmain = QtGui.QVBoxLayout()
    logLayoutmain.addLayout(loginLayout)
    logLayoutmain.addStretch()
    logLayoutmain.addLayout(pwdLayout)

    statusLabel = QtGui.QLabel()
    statusLabel.setText("Addresses Fetched")
    statusLabel.setAlignment(QtCore.Qt.AlignCenter)
    statusFont = QtGui.QFont()
    statusFont.setBold(True)
    statusFont.setItalic(True)
    statusFont.setFamily("Helvetica")
    statusFont.setPointSize(15)
    statusLabel.setFont(statusFont)
    statusLayout = QtGui.QHBoxLayout()
    statusLayout.addStretch()
    statusLayout.addWidget(statusLabel)
    statusLayout.addStretch()

    registerButton = QtGui.QPushButton()
    registerButton.setText("Register Your System")
    registerButton.clicked.connect(lambda : callService(str(loginInput.text()), str(pwdInput.text()), statusLabel))
    registerButtonLayout = QtGui.QHBoxLayout()
    registerButtonLayout.addStretch()
    registerButtonLayout.addWidget(registerButton)
    registerButtonLayout.addStretch()

    exitButton = QtGui.QPushButton()
    exitButton.setText("Exit")
    exitButton.clicked.connect(exitFunc)
    exitLayout = QtGui.QHBoxLayout()
    exitLayout.addStretch(4)
    exitLayout.addWidget(exitButton)

    mainLayout = QtGui.QVBoxLayout()
    mainLayout.addLayout(titleLayout)
    mainLayout.addStretch()
    mainLayout.addLayout(addrLayout)
    mainLayout.addStretch()
    mainLayout.addLayout(logLayoutmain)
    mainLayout.addStretch()
    mainLayout.addLayout(registerButtonLayout)
    mainLayout.addStretch()
    mainLayout.addLayout(statusLayout)
    mainLayout.addStretch()
    mainLayout.addLayout(exitLayout)
    homeLayout = QtGui.QFrame()
    homeLayout.setLayout(mainLayout)
    mainWindow.setCentralWidget(homeLayout)
    mainWindow.show()
    sys.exit(app.exec_())

if __name__ == '__main__' :
    findAddress()
    mainWin()